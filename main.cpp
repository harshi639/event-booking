#include "classes.h"
#include "iostream"
// #include "stdlib.h"
// #include "cstdio"
// #include "string"
// #include "string"
#include <bits/stdc++.h>
#include <cstdio>

using namespace std;

// int main(){
// 	Event o;
// 	o.setTypeOfEvent("Live Music");
// 	o.showTypeOfEvent();
// 	return 0;
// }

//****************************MAIN SECTION***********************************
char c;
Event event_obj;
LiveMusic music_obj;
StandUpComedy comedy_obj;
Film film_obj;

void makeBookingChoice(){
		system("clear");
		
		char c;
		event_obj.listAllEvents();
		cout<<"Which Event you want to book (choose from above list)"<<endl;
		cin.ignore();
		c=getchar();
		
		if (isdigit((char)c)==1  ){
			switch(c){
				case '1':
					music_obj.addBookingMusic();
					break;

				case '2':
					comedy_obj.addBookingComedy();
					break;
				
				case '3':
					film_obj.addBookingFilm();
					break;
					
				case '0':
					return;

				default:
					cout<<"Wrong Choice selected"<<endl;
					
			}
		}
		
		else{
		cout<<"Please enter numerical digit only"<<endl;

		}
}

void makeTotalCount(){
		system("clear");
		
		char c;
		event_obj.listAllEvents();
		cout<<"Which Event Total you want to Check (choose from above list)"<<endl;
		cin.ignore();
		c=getchar();
		int left;
		if (isdigit((char)c)==1  ){
			switch(c){
				case '1':
					left=event_obj.checkSeatsAvailability(1);
					cout<<"\nTotal Positions in Live Music=300"<<endl;
					cout<<"\nSeats Left in Live Music="<<left<<endl;
					c=getchar();
					break;

				case '2':
					left=event_obj.checkSeatsAvailability(2);
					cout<<"\nTotal Positions in Stand up comedy=200"<<endl;
					cout<<"\nSeats Left in Stand up comedy="<<left<<endl;
					c=getchar();
					break;
				
				case '3':
					left=event_obj.checkSeatsAvailability(3);
					cout<<"\nTotal Positions in Film=200"<<endl;
					cout<<"\nSeats Left in Film="<<left<<endl;
					c=getchar();
					break;
					
				case '0':
					return;

				default:
					cout<<"Wrong Choice selected"<<endl;
					
			}
		}
		
		else{
		cout<<"Please enter numerical digit only"<<endl;

		}
}

void makeDisplay(){
		system("clear");
		
		char c;
		event_obj.listAllEvents();
		cout<<"Which Event List you want to display (choose from above list)"<<endl;
		cin.ignore();
		c=getchar();
		cout<<"\n\n";
		if (isdigit((char)c)==1  ){
			switch(c){
				case '1':
					music_obj.showListBookingMusic();
					break;

				case '2':
					comedy_obj.showListBookingComedy();
					break;
				
				case '3':
					film_obj.showListBookingFilm();
					break;
					
				case '0':
					return;

				default:
					cout<<"Wrong Choice selected"<<endl;
					
			}
		}
		
		else{
		cout<<"Please enter numerical digit only"<<endl;

		}
}

int main()
{

  char ch;
  system("clear");
  cout<<"\n\t\tWELCOME TO EVENT BOOKING SYSTEM \n";
  cout<<"\n\t\tPress any key to continue\n";
  
  comedy_obj.setArraySeatFalse();
  music_obj.setPositionFalse();
  while(1)
  {
  system("clear");

  cout<<"\n\t\t *****EVENT BOOKING SYSTEM***** \n";
  cout<<"\n\t\t1.List All Events ";
  cout<<"\n\t\t2.Add Booking for an Event ";
  cout<<"\n\t\t3.Display the list of an event ";
  cout<<"\n\t\t4.Count Total bookings of an event ";
  cout<<"\n\t\t0.EXIT ! ";
  cout<<"\n\t\tENTER YOUR CHOICE: ";
  ch=getchar();

  if (isdigit(ch)==1 ){

	switch(ch)
	{
		case '1': 
		cout<<"\n\tList Of All The Events is given below:"<<endl;
		event_obj.listAllEvents();
			break;

		case '2': //close_ac();

  		makeBookingChoice();
			break;

		case '3': //modify_ac();
  		makeDisplay();
			break;
		
		case '4': //modify_ac();
  		makeTotalCount();
			break;

		case '0': cout<<"\n\t\tThe program is EXITED... bye bye!\n";
			c=getchar();
			exit(0);

		default:
		cout<<"\n\t\t\tYou have entered WRONG choice\n";
		break;

		}
		
	c=getchar();
	c=getchar();
	}
	else {
		cout<<"Please enter one digit only"<<endl;
	}
}

    return 0;
}
