#include <iostream>

#ifndef BASECLASS_H
#define BASECLASS_H

using namespace std;

class Event{
	private:
	string typeofevent;
	static const int total_musicseats=300;
	static const int total_comedyseats=200;
	static const int total_filmseats=200;

	public:
	void setNumberOfSeats(int );
	void setTypeOfEvent(string );
	void setSeatFalse();
	void showTypeOfEvent();
	void listAllEvents();
	bool checkMobile(string);
	int checkSeatsAvailability(int);
	bool checkBookingId(int);
	int getTotalMusicPositions();
	int getTotalComedySeats();
	int getTotalFilmSeats();

};



class LiveMusic:public Event{

	private:
	
	string music_bookingid;
	string name;
	string mobile;
	int price;
	string eventtype ;
	int position;
	bool arrayposition[300];
	


	public:
	
	void setPositionFalse();
	void addBookingMusic();
	void showListBookingMusic();
	void showArrayPosition();
};

class StandUpComedy:public Event{
	
	private:
	
	string comedy_bookingid;
	string name;
	string mobile;
	int price;
	string eventtype ;
	int seat;
	bool arrayseat[200];
	
	
	public:
	void setArraySeatFalse();
	void showArraySeat();
	void addBookingComedy();
	void showListBookingComedy();

};

class Film:public Event{
	
	private:

	
	int seatsleft;
	string film_bookingid;
	string name;
	string mobile;
	int price;
	string eventtype ;
	string seattype;

	public:
	
	
	void setFilmBookingId(string);
	void setFilmSeatType(string);
	void addBookingFilm();
	void showListBookingFilm();


};

#endif  //BASECLASS_H

