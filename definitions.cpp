#include "classes.h"
#include "iostream"
#include <bits/stdc++.h>
#include "cstdio"
#include "string"
using namespace std;

char cc;

void LiveMusic::showArrayPosition(){
	for(int i=0;i<300;i++){
		cout<<arrayposition[i]<<endl;
	}
}

void StandUpComedy::showArraySeat(){
	for(int i=0;i<200;i++){
		cout<<arrayseat[i]<<endl;
	}
}

bool Event::checkMobile(string s){

	if (s.length()!=10){
		cout<<"Mobile should contain 10 digits"<<endl;
		return 1;
	}
}

int Event::checkSeatsAvailability(int n){
	if (n==1){  //checking availabilty for live music event
		string line;
		int count=0;
		ifstream ff("LiveMusic.txt",ios::in);

		while(ff){
			getline(ff,line);
			count+=1;
		}
		int t=getTotalMusicPositions();
		return (t-count); 
	}
	else if (n==2){ //checking availabilty for stand up comedy event
		string line;
		int count=0;
		ifstream ff("StandUpComedy.txt",ios::in);

		while(ff){
			getline(ff,line);
			count+=1;
		}
		int t=getTotalComedySeats();
		return (t-count); 
	}
	else if (n==3){ //checking availabilty for film event

		string line;
		int count=0;
		ifstream ff("Film.txt",ios::in);

		while(ff){
			getline(ff,line);
			count+=1;
		}
		int t=getTotalFilmSeats();
		return (t-count); 
	
	}
}



void Event::listAllEvents(){

	cout<<"\t1.Live Music "<<endl;
	cout<<"\t2.Stand up Comedy"<<endl;
	cout<<"\t3.Film\n"<<endl;
	//cc=getchar();
}

void Event::setTypeOfEvent(string e){
	typeofevent=e;
}

void Event::showTypeOfEvent(){
	cout<<"Type="<<typeofevent<<endl;
}

int Event::getTotalMusicPositions(){
	return total_musicseats;
}
int Event::getTotalComedySeats(){
	return total_comedyseats;
}
int Event::getTotalFilmSeats(){
	return total_filmseats;
}


void LiveMusic::setPositionFalse(){
	for(int i=0;i<300;i++){
		arrayposition[i]=0;
	}
}

void LiveMusic::addBookingMusic(){

	cout<<"Enter Live-Music Booking Id:";
	cin>>music_bookingid;
	cin.ignore();
	cout<<"Enter Name:";
	getline(cin,name);
	

	cout<<"Enter Mobile:";
	cin>>mobile;
	if (checkMobile(mobile)==1){
		return;
	}
	eventtype="LiveMusic";
	cout<<"Enter Price:";
	cin>>price;
	cout<<"Enter Position:";

	cin>>position;
	if (arrayposition[position-1]==1){
		cout<<"Position already booked"<<endl;
	}
	else if (position<1 || position>300){
		cout<<"Position should be between 1 to 300"<<endl;

	}
else{
	arrayposition[position-1]=1;
	ofstream lmf("LiveMusic.txt",ios::app);
	lmf<<music_bookingid<<" "<<name<<" "<<mobile<<" "<<eventtype<<" "<<price<<" "<<position<<"\n";
	lmf.close();
	cout<<"\nEvent Booked Successfully!\n";
}

}



void StandUpComedy::setArraySeatFalse(){
	string line;
	int t=getTotalComedySeats();
	for(int i=0;i<t;i++){
		arrayseat[i]=0;
	}
		
		int count=0;

		cc=getchar();
}

void StandUpComedy::addBookingComedy(){

	cout<<"Enter Stand Up Comedy Booking Id:";
	cin>>comedy_bookingid;
	cin.ignore();
	cout<<"Enter Name:";
	getline(cin,name);
	

	cout<<"Enter Mobile:";
	cin>>mobile;
	if (checkMobile(mobile)==1){
		return;
	}
	eventtype="StandUpComedy";
	cout<<"Enter Price:";
	cin>>price;
	cout<<"Enter Seat Number:";
	cin>>seat;
	if (arrayseat[seat-1]==1){
		cout<<"Position already booked"<<endl;
	}
	else if (seat<1 || seat>300){
		cout<<"Seat number should be between 1 to 200"<<endl;

	}
else{
	arrayseat[seat-1]=1;
	ofstream scf("StandUpComedy.txt",ios::app);
	scf<<comedy_bookingid<<" "<<name<<" "<<mobile<<" "<<eventtype<<" "<<price<<" "<<seat<<"\n";
	scf.close();
	cout<<"\nEvent Booked Successfully!\n";
}

}

void LiveMusic::showListBookingMusic(){

	
			string line;
			
			ifstream ff("LiveMusic.txt",ios::in);
			if (!ff){
				cout<<"Sorry no data available"<<endl;
				cc=getchar();
				return;
			}
			while(ff){
				getline(ff,line);
				cout<<line<<endl;
			}
			
		
}
void StandUpComedy::showListBookingComedy(){

	
			string line;
			
			ifstream ff("StandUpComedy.txt",ios::in);
			if (!ff){
				cout<<"Sorry no data available"<<endl;
				cc=getchar();
				return;
			}
			while(ff){
				getline(ff,line);
				cout<<line<<endl;
			}
			
		
}
void Film::showListBookingFilm(){

	
			string line;
			
			ifstream ff("Film.txt",ios::in);
			if (!ff){
				cout<<"Sorry no data available"<<endl;
				cc=getchar();
				return;
			}
			while(ff){
				getline(ff,line);
				cout<<line<<endl;
			}
			
		
}
void Film::addBookingFilm(){

	int seatsleft = checkSeatsAvailability(3);
	if (seatsleft<=0){
		cout<<"Sorry,your booking cannot be completed, The show is housefull!!"<<endl;
		return;
	}
	cout<<"Enter Stand Up Comedy Booking Id:";
	cin>>film_bookingid;
	cin.ignore();
	cout<<"Enter Name:";
	getline(cin,name);
	

	cout<<"Enter Mobile:";
	cin>>mobile;
	if (checkMobile(mobile)==1){
		return;
	}
	eventtype="Film";
	cout<<"Enter Seat Type:";
	cin>>seattype;
	if (seattype!="2D" || seattype!="3D"){
		cout<<"Film Seat Type can only be 2D or 3D"<<endl;
		return;
	}
	cout<<"Enter Price:";
	cin>>price;
	


	ofstream ff("StandUpComedy.txt",ios::app);
	ff<<film_bookingid<<" "<<name<<" "<<mobile<<" "<<eventtype<<" "<<seattype<<" "<<price<<"\n";
	ff.close();
	cout<<"\nEvent Booked Successfully!\n";
}

